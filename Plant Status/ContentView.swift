//
//  ContentView.swift
//  Plant Status
//
//  Created by Axel Roest on 25-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    @ObservedObject var backend = PlantBackend()
    @State private var warning: Bool = false

    var body: some View {
        
        VStack(spacing: 0.0) {
            if backend.warning {
                WarningView(warning: backend.warning)
                .frame(height: 40)
            }
            List {
                // try to make this a binding
                ForEach(backend.sensors, id: \.id) { sensor in
                    PlantView(sensor: sensor)
                }
                .listRowBackground(Color.cellBackground)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
