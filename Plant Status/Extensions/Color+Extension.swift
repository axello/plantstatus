//
//  Color+Extension.swift
//  Plant Status
//
//  Created by Axel Roest on 26-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import SwiftUI

public extension Color {
    static let cellBackground = Color(#colorLiteral(red: 0.9167807102, green: 0.9167807102, blue: 0.9167807102, alpha: 1))
}
