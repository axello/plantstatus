//
//  PlantStatusView.swift
//  Plant Status
//
//  Created by Axel Roest on 26-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import SwiftUI

struct PlantStatusView: View {
    let status: SensorStatus
    
    func statusView(_ status: SensorStatus) -> some View {
        ZStack() {
            Rectangle()
                .foregroundColor(Color.white)
                .cornerRadius(10)
                .padding(5)
                .background(status.statusColor)
                .cornerRadius(15.0)
                .frame(width: 100, height: 100, alignment: .center)
            Text(status.statusIcon)
                .background(Color.white)
                .font(.system(size: 60))
        }
    }
    
    var body: some View {
        statusView(status)
    }
    
    init(_ status: SensorStatus) {
        self.status = status
    }
}

struct PlantStatusView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PlantStatusView(.unknown)
            PlantStatusView(.tooDry)
            PlantStatusView(.perfect)
        }
    }
}
