//
//  PlantView.swift
//  Plant Status
//
//  Created by Axel Roest on 25-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import SwiftUI

// todo add refreshableScrollview
struct PlantView: View {
    var sensor: Sensor
    
    var body: some View {
        HStack(alignment: .top) {
            RemoteImage(url: URL(string: sensor.imageUrl ?? ""), image: UIImage(named: "flowerpot.jpg")!)
                .cornerRadius(10)
                .padding(5)
                .background(sensor.status.statusColor)
                .cornerRadius(15.0)
                .frame(width: 100, height: 100, alignment: .center)
            VStack(alignment: .leading) {
                Text(verbatim: sensor.name)
                    .padding(.top, 8)
                Spacer()
                HStack {
                    Text("Value: \(sensor.moisture)")
                        .foregroundColor(Color.secondary)
                        .padding(.trailing)
                    .padding(.bottom, 8)
                }
            }.alignmentGuide(.top) { (what) -> CGFloat in
                0
            }
            Spacer()
            PlantStatusView(sensor.status)
            
        }.background(Color.clear)
            .padding(.vertical, 8)
    }
    
    init(sensor: Sensor) {
        self.sensor = sensor
    }
}

struct PlantView_Previews: PreviewProvider {
    static var previews: some View {
        let reading = Reading.mockReading(sensorId: 1, tooDry: 300)
        let sensor1 = Sensor(id: 1,
                             name: "Rosemarinus officionalis",
                             imageUrl: "sansevieria",
                             tooDryAt: 210)
        sensor1.addReading(reading)
        let sensor2 = Sensor(id: 1,
                             name: "Sansevieria",
                             imageUrl: "sansevieria",
                             tooDryAt: 310)
        sensor2.addReading(reading)
        return Group {
            PlantView(sensor: sensor1)
            PlantView(sensor: sensor2)
            PlantView(sensor: Sensor(id: 1, name: "Sansevieria", imageUrl: "sansevieria", tooDryAt: 440))
        }
    }
}
