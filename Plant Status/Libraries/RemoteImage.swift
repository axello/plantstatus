//
//  RemoteImage.swift
//  Plant Status
//
//  https://onmyway133.github.io/blog/How-to-load-remote-image-in-SwiftUI/
//

import SwiftUI

struct RemoteImage: View {
    let url: URL?
    let imageLoader = ImageLoader()
    @State var image: UIImage? = nil

    var body: some View {
        Group {
            makeContent()
        }
        .onReceive(imageLoader.objectWillChange, perform: { image in
            self.image = image
        })
        .onAppear(perform: {
            if let url = url {
                self.imageLoader.load(url: url)
            }
        })
        .onDisappear(perform: {
            self.imageLoader.cancel()
        })
    }

    private func makeContent() -> some View {
        if let image = image {
            return AnyView(
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            )
        } else {
            return AnyView(Text("😢"))
        }
    }
}
