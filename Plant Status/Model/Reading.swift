//
//  Reading.swift
//  Plant Status
//
//  Created by Axel Roest on 25-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import Foundation

final class Reading: Identifiable {
    public private(set) var id: Int
    public private(set) var sensor: Int
    public private(set) var date: Date
    public private(set) var value: Double
    
    init(id: Int, sensor: Int, date: Date, value: Double ) {
        self.id = id
        self.sensor = sensor
        self.date = date
        self.value = value
    }
    
    class func mockReading(sensorId: Int, tooDry: Double = Double.random(in: 320...400)) -> Reading {
        Reading(id: Int.random(in: 1...1_000_000), sensor: sensorId, date: Date(), value: tooDry)
    }
}
