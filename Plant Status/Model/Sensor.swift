//
//  Sensor.swift
//  Plant Status
//
//  Created by Axel Roest on 25-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import UIKit
import SwiftUI

final class Sensor: ObservableObject, Identifiable {
    public private(set) var id: Int
    var name: String
    var drySetpoint: Double
    public private(set) var imageUrl: String? {
        didSet {
            if let url = URL(string: imageUrl ?? "") {
                self.fetchImage(url)
            }
        }
    }
    @Published var image: UIImage = UIImage(named: "flowerpot.jpg")!

    public private(set) var readings = [Reading]()
    
    init(id: Int, name: String, imageUrl: String? = nil, tooDryAt: Double) {
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
        drySetpoint = tooDryAt
    }
    
    func addReading(_ reading: Reading) {
        readings.append(reading)
    }
    
    public var status: SensorStatus {
        get {
            guard let result = readings.last?.value else {
                return .unknown
            }
            if result < drySetpoint {
                return .tooDry
            }
            return .perfect
        }
    }
    
    public var moisture: String {
        get {
            guard let value = readings.last?.value else {
                return "?"
            }
            return String(Int(value))
        }
    }
}

private extension Sensor {
    func fetchImage(_ url: URL)
    {
        DispatchQueue.main.async {
            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    self.image = image
                }
            }
            catch {
                
            }
        }
    }
}

extension Sensor: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageUrl = "image"
        case drySetpoint
    }
}
