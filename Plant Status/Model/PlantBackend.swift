//
//  PlantBackend.swift
//  Plant Status
//
//  Created by Axel Roest on 25-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import Foundation
import Combine

enum PlantService: String {
    case sensors
    case readings
}

fileprivate(set) var urlComponents: URLComponents = {
    var components = URLComponents()
    // these details may change!
    components.scheme = "http"
    components.port = 8080
    components.host = "plants.roeroe.nl"
//    components.host = "rockpaperscissorsserver.herokuapp.com"
    return components
}()

func setURLPath(_ path: String) {
    urlComponents.path = path
}

class PlantBackend: ObservableObject {
    var cancellable: AnyCancellable?
    
    @Published var sensors: [Sensor] = []
    
    var warning: Bool {
        let status = sensors.map{$0.status}.reduce(SensorStatus.unknown, {
            if $1 == SensorStatus.tooDry {
                return SensorStatus.tooDry
            }
            return $0
        })
        return status == .tooDry
    }
    
    init() {
        defer {
            fetchSensors()
        }
    }

    func mockFetch() {
        let s1 = Sensor(id: 1, name: "Sansevieria", imageUrl: "sansevieria", tooDryAt: 310)
        let s2 = Sensor(id: 2, name: "Aloë Vera", imageUrl: "aloevera", tooDryAt: 305)
        s1.addReading(Reading.mockReading(sensorId: 1))
        s2.addReading(Reading.mockReading(sensorId: 2, tooDry: 200))
        sensors = [s1, s2]
    }
    
    func fetchSensors() {
        guard let myHost = urlComponents.url else {return}
        let myURL = myHost.appendingPathComponent(PlantService.sensors.rawValue)
        
        cancellable = URLSession.shared
            .dataTaskPublisher(for: myURL)
            .map{$0.data}
            .decode(type: [Sensor].self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            // sink the information, as there might be errors
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        print("finished")
                    case .failure(let error):
                        print("error: \(error)")
                    }
            }, receiveValue: { sensors in
                self.sensors = sensors
            }
        )
    }
    
    func fetchReadings() {
        guard let myHost = urlComponents.url else {return}
        let myURL = myHost.appendingPathComponent(PlantService.sensors.rawValue)
        
        cancellable = URLSession.shared
            .dataTaskPublisher(for: myURL)
            .map{$0.data}
            .decode(type: [Sensor].self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            // sink the information, as there might be errors
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        print("finished")
                    case .failure(let error):
                        print("error: \(error)")
                    }
            }, receiveValue: { sensors in
                self.sensors = sensors
            }
        )

    }

}
