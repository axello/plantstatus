//
//  SensorStatus.swift
//  Plant Status
//
//  Created by Axel Roest on 05-03-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import UIKit
import SwiftUI

enum SensorStatus {
    case unknown
    case perfect
    case tooDry
    
    var statusIcon: String {
        get {
            switch self {
            case .unknown:
                return "🕵️‍♀️"
            case .perfect:
                return "😋"
            case .tooDry:
                return "💦"
            }
        }
    }
    
    var statusColor: Color {
        switch self {
        case .tooDry:
            return Color.red
        case .unknown:
            return Color.secondary
        case .perfect:
            return Color.green
        }
    }
}
