//
//  WarningView.swift
//  Plant Status
//
//  Created by Axel Roest on 26-02-2020.
//  Copyright © 2020-2022 Phluxus. All rights reserved.
//

import SwiftUI

struct WarningView: View {
    let warning: Bool
    
    var body: some View {
        GeometryReader { proxy in
            ZStack(alignment:
                    .center) {
                HStack(spacing: 0) {
                    ForEach(0..<40) { number in
                        Rectangle()
                            .fill((number % 2 == 0) ? Color.black : Color.yellow)
                    }
                    .transformEffect(.init(rotationAngle: 0.3))
                }
                .frame(width: proxy.size.width * 2.0, height: proxy.size.height * 1.5)
                HStack(alignment: .center, spacing: 0.0) {
                    Spacer()
                    if self.warning {
                        Text("WARNING")
                            .font(.largeTitle)
                            .bold()
                            .multilineTextAlignment(.center)
                            .padding(8)
                            .background(Color.white.opacity(0.8))
                            .clipShape(Capsule())

                    }
                    Spacer()
                }
            }
            // add a cliprectangle
            .frame(width: proxy.size.width, height: proxy.size.height)
        }
        .clipped()
    }
    
    init(warning: Bool) {
        self.warning = warning
    }
 }

struct WarningView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            WarningView(warning: true)
                .frame(width: 200, height: 40)
            
            WarningView(warning: false)
                .frame(width: 200, height: 60)
        }
    }
}
